const mongoose = require("mongoose")

// Mongoose Schema for Templates
const TemplateSchema = new mongoose.Schema({
  subcategoryId: {
    type: mongoose.Types.ObjectId,
    required: true,
    unique: true,
  },
}, { timestamps: true })

module.exports = {
  TemplateSchema,
}
