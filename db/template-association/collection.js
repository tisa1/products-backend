const mongoose = require("mongoose")
const { TemplateSchema } = require("./schema")

const Template = mongoose.model("templates", TemplateSchema)

module.exports = {
  Template,
}
