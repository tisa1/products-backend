const responses = {
  CATEGORY_CREATED: "categoryCreated",
  SUBCATEGORY_CREATED: "subCategoryCreated",
  NO_CATEGORY_ID: "noCategoryId",
  CATEGORY_ID_INVALID: "categoryIdInvalid",
  NO_CATEGORY: "noCategory",
  NO_NAME: "noName",
  NAME_TAKEN: "nameTaken",
  TEMPLATE_DELETED: "templateDeleted",
}

module.exports = {
  responses,
}
