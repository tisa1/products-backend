const mongoose = require("mongoose")
const _ = require("underscore")
const { fieldTypes } = require("./constants")

const allowedFieldTypes = _.map(fieldTypes, (t) => t)

// Mongoose Schema for TempInputs
const TempInputSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    trim: true,
  },
  dataType: {
    type: String,
    enum: allowedFieldTypes,
    required: true,
  },
  meta: {
    select: {
      options: {
        type: [String],
        required() {
          return this.dataType === fieldTypes.SELECT
        },
      },
    },
  },
}, { timestamps: true })

module.exports = {
  TempInputSchema,
}
