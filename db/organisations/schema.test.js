const mongoose = require("mongoose")
const { Org } = require("db/organisations/collection")

describe("organisations", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Org.deleteMany({})
  })

  it("should create an org", async () => {
    const data = {
      name: "Tisa",
      userId: "601950f833c741001d10a347",
    }

    await new Org(data).save()
    const num = await Org.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert an empty user", async () => {
    const fakeOrg = {}
    let error = null
    try {
      await new Org(fakeOrg).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
