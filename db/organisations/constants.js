const responses = {
  ORGANISATION_CREATED: "organisationCreated",
  NO_USER_ID: "noUserId",
  USER_ID_INVALID: "userIdInvalid",
  NO_USER_FOUND: "noUserFound",
  NO_NAME: "noName",
  NAME_TAKEN: "nameTaken",
  ORGANISATION_DELETED: "organisationDeleted",
  NO_ORGANISATION_ID: "noOrganisationId",
  NOT_ALLOWED: "notAllowed",
}

module.exports = {
  responses,
}
