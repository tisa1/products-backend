const roles = {
  OWNER: "owner",
  ADMIN: "admin",
  MEMBER: "member",
}

module.exports = {
  roles,
}
