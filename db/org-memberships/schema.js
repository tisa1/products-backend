const mongoose = require("mongoose")

const _ = require("underscore")
const { roles } = require("./constants")

const allowedRoles = _.map(roles, (a) => a)

// Mongoose Schema for OrgMembership
const OrgMembershipSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: allowedRoles,
  },
  orgId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
},
{ timestamps: true })

module.exports = {
  OrgMembershipSchema,
}
