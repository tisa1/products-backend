const mongoose = require("mongoose")
const { Product } = require("db/products/collection")

describe("products", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Product.deleteMany({})
  })

  it("should create a product", async () => {
    const data = {
      subcategoryId: "601950f833c741001d10a347",
      organisationId: "601950f833c741001d10a347",
    }

    await new Product(data).save()
    const num = await Product.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert an empty product", async () => {
    const fakeProduct = {}
    let error = null
    try {
      await new Product(fakeProduct).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
