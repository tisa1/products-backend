const responses = {
  TEMP_INP_ASSOC_CREATED: "templateInputAssocCreated",

  NO_TEMPLATE_ID: "noTemplateId",
  TEMPLATE_ID_INVALID: "templateIdInvalid",
  NO_TEMPLATE: "noTemplate",

  NO_TEMP_INPUT_ID: "noTemplateInputId",
  TEMP_INPUT_ID_INVALID: "templateInputIdInvalid",
  NO_TEMP_INPUT: "noTemplateInput",

}

module.exports = {
  responses,
}
