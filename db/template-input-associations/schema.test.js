const mongoose = require("mongoose")
const { Template } = require("db/templates/collection")

describe("templates", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Template.deleteMany({})
  })

  it("should create a template", async () => {
    const data = {
      subcategoryId: "601950f833c741001d10a347",
    }

    await new Template(data).save()
    const num = await Template.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert an empty template", async () => {
    const fakeTemplate = {}
    let error = null
    try {
      await new Template(fakeTemplate).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
