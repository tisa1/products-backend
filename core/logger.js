// const logDnaWinston = require("logdna-winston");
// const logDnaOptions = require("middleware/logDna")

const winston = require("winston")

const logger = winston.createLogger({
  transports: [
    // new (logDnaWinston)(logDnaOptions),
    new (winston.transports.Console)(),
    new winston.transports.File({ filename: "error.log", level: "error" }),
    new winston.transports.File({ filename: "out.log" }),
  ],
})

const error = ({ message, data }) => {
  logger.log({ level: "error", message, data })
}

const info = ({ message, data }) => {
  logger.log({ level: "info", message, data })
}

module.exports = {
  logger,
  error,
  info,
}
