const mongoose = require("mongoose")
const { Product } = require("db/products/collection")
const { Org } = require("db/organisations/collection")
const { OrgMembership } = require("db/org-memberships/collection")
const { Category, SubCategory } = require("db/categories/collection")
const OrgMembershipConstants = require("db/org-memberships/constants")

const User = require("db/users/collection")

const app = require("middleware/app")
const { responses } = require("db/products/constants")
const config = require("config")
const supertest = require("supertest")

const productRouter = require("routes/products/route")()
const jwt = require("jsonwebtoken")

const request = supertest(app)

const userData = {
  profile: {
    firstName: "Michael",
    lastName: "Perju",
    birthdate: new Date(),
  },
  email: "admin@app.com",
  password: "danPot9_ij",
}

const login = (user) => jwt.sign(
  { email: user.email, userId: user?._id },
  config.jwt.secret,
  {
    expiresIn: config.jwt.expiresIn,
  },
)

const createUser = async (data = userData) => new User(data).save()

const createOrg = async (user, data = { name: "test", role: OrgMembershipConstants.roles.OWNER }) => {
  if (!user) user = await createUser()

  const organisation = await new Org(data).save()
  const orgMembership = await new OrgMembership({
    userId: user._id,
    orgId: organisation._id,
    role: data.role,
  }).save()

  return { organisation, orgMembership }
}

const createCategory = async (data = { name: "Vehicles" }) => new Category(data).save()

const createSubCategory = async (category, data = { name: "Cars" }) => {
  if (!category) category = await createCategory()

  return new SubCategory({ categoryId: category._id, ...data }).save()
}

describe("templates route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Product.deleteMany()
    await Org.deleteMany()
    await OrgMembership.deleteMany()
    await User.deleteMany()
    await Category.deleteMany()
    await SubCategory.deleteMany()
  })

  it("a request without the organisation id should fail", async () => {
    app.use("/", productRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    const res = await request.post("/")
      .send({})
      .set("Authorization", `Bearer ${token1}`)
      .expect(400)

    expect(res.body.message).toBe(responses.NO_ORGANISATION_ID)
  })

  it("a request with an invalid organisation id should fail", async () => {
    app.use("/", productRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    const res = await request.post("/")
      .send({ organisationId: "fake" })
      .set("Authorization", `Bearer ${token1}`)
      .expect(400)

    expect(res.body.message).toBe(responses.ORGANISATION_ID_INVALID)
  })

  it("a request with a non-existing organisation should fail", async () => {
    app.use("/", productRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    const res = await request.post("/")
      .send({ organisationId: "60895a1935f3351aabb10a2d" })
      .set("Authorization", `Bearer ${token1}`)
      .expect(400)

    expect(res.body.message).toBe(responses.NO_ORGANISATION)
  })

  it("having a valid organisation id in place, the request should not fail because of it", async () => {
    app.use("/", productRouter)

    const user1 = await createUser()
    const { organisation } = await createOrg(user1)
    const token1 = login(user1)

    const res = await request.post("/")
      .send({ organisationId: organisation._id })
      .set("Authorization", `Bearer ${token1}`)

    expect(res.body.message).not.toBe(responses.NO_ORGANISATION)
    expect(res.body.message).not.toBe(responses.NO_ORGANISATION_ID)
    expect(res.body.message).not.toBe(responses.ORGANISATION_ID_INVALID)
  })

  it("a non-member of an organisation cannot create products on its behalf", async () => {
    app.use("/", productRouter)

    const subcategory = await createSubCategory()

    const user1 = await createUser()
    const user2 = await createUser({ email: "admin2@app.com", ...userData })

    const orgRes1 = await createOrg(user1)
    const orgRes2 = await createOrg(user2, { name: "test2", role: OrgMembershipConstants.roles.OWNER })

    const token1 = login(user1)
    const token2 = login(user2)

    // first user cannot create products for the first company
    let res = await request.post("/")
      .send({
        organisationId: orgRes1.organisation._id,
        subcategoryId: subcategory._id,
      })
      .set("Authorization", `Bearer ${token2}`)
      .expect(403)

    expect(res.body.message).toBe(responses.NO_ORGANISATION_MEMBERSHIP)

    // second user cannot create products for the second company
    res = await request.post("/")
      .send({
        organisationId: orgRes2.organisation._id,
        subcategoryId: subcategory._id,
      })
      .set("Authorization", `Bearer ${token1}`)
      .expect(403)

    expect(res.body.message).toBe(responses.NO_ORGANISATION_MEMBERSHIP)
  })

  it("a member of an organisation with non-admin role cannot create products on its behalf", async () => {
    app.use("/", productRouter)

    const subcategory = await createSubCategory()
    const user1 = await createUser()
    const orgRes1 = await createOrg(user1, { name: "Test", role: OrgMembershipConstants.roles.MEMBER })
    const token1 = login(user1)

    // first user cannot create products for the first company
    const res = await request.post("/")
      .send({
        organisationId: orgRes1.organisation._id,
        subcategoryId: subcategory._id,
      })
      .set("Authorization", `Bearer ${token1}`)
      .expect(403)

    expect(res.body.message).toBe(responses.NO_ORGANISATION_MEMBERSHIP)
  })

  it("a request without the subcategory id should fail", async () => {
    app.use("/", productRouter)

    const user1 = await createUser()
    const { organisation } = await createOrg(user1)
    const token1 = login(user1)

    const res = await request.post("/")
      .send({ organisationId: organisation._id })
      .set("Authorization", `Bearer ${token1}`)
      .expect(400)

    expect(res.body.message).toBe(responses.NO_SUBCATEGORY_ID)
  })

  it("a request with an invalid subcategory id should fail", async () => {
    app.use("/", productRouter)

    const user1 = await createUser()
    const { organisation } = await createOrg(user1)
    const token1 = login(user1)

    const res = await request.post("/")
      .send({ organisationId: organisation._id, subcategoryId: "fake" })
      .set("Authorization", `Bearer ${token1}`)
      .expect(400)

    expect(res.body.message).toBe(responses.SUBCATEGORY_ID_INVALID)
  })

  it("a request with an non-existing subcategory should fail", async () => {
    app.use("/", productRouter)

    const user1 = await createUser()
    const { organisation } = await createOrg(user1)
    const token1 = login(user1)

    const res = await request.post("/")
      .send({ organisationId: organisation._id, subcategoryId: "60895a1935f3351aabb10a2d" })
      .set("Authorization", `Bearer ${token1}`)
      .expect(400)

    expect(res.body.message).toBe(responses.NO_SUBCATEGORY)
  })

  it("given a valid subcategory id, the request should not fail because of it", async () => {
    app.use("/", productRouter)

    const user1 = await createUser()
    const { organisation } = await createOrg(user1)
    const subcategory = await createSubCategory()
    const token1 = login(user1)

    const res = await request.post("/")
      .send({ organisationId: organisation._id, subcategoryId: subcategory._id })
      .set("Authorization", `Bearer ${token1}`)

    expect(res.body.message).not.toBe(responses.NO_SUBCATEGORY)
    expect(res.body.message).not.toBe(responses.NO_SUBCATEGORY_ID)
    expect(res.body.message).not.toBe(responses.SUBCATEGORY_ID_INVALID)
  })
})
