const mongoose = require("mongoose")
const { Product } = require("db/products/collection")
const User = require("db/users/collection")

const app = require("middleware/app")
const { responses } = require("db/templates/constants")
const config = require("config")
const supertest = require("supertest")

const productRouter = require("routes/products/route")()
const jwtSecurity = require("core/jwt")
const jwt = require("jsonwebtoken")

const request = supertest(app)
const _ = require("underscore")

const userData = {
  profile: {
    firstName: "Michael",
    lastName: "Perju",
    birthdate: new Date(),
  },
  email: "admin@app.com",
  password: "danPot9_ij",
}

const login = (user) => jwt.sign(
  { email: user.email, userId: user?._id },
  config.jwt.secret,
  {
    expiresIn: config.jwt.expiresIn,
  },
)

describe("products route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Product.deleteMany()
  })

  it("creating an product without a token should not work", async () => {
    app.use("/", productRouter)

    const res = await request.post("/")
      .send({})
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.NO_TOKEN)
  })

  it("creating a product with an invalid token should not work", async () => {
    app.use("/", productRouter)

    const res = await request.post("/")
      .send({})
      .set("Authorization", "Bearer XXXXXX")
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.TOKEN_INVALID)
  })

  it("given a valid token, the request might fail, but not due jwt validation", async () => {
    app.use("/", productRouter)

    const user1 = await new User(userData).save()
    const token1 = login(user1)

    const res = await request.post("/")
      .send({})
      .set("Authorization", `Bearer ${token1}`)

    expect(res.body.message).not.toBe(jwtSecurity.responses.TOKEN_INVALID)
    expect(res.body.message).not.toBe(jwtSecurity.responses.NO_TOKEN)
  })
})
