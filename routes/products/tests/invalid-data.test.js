const mongoose = require("mongoose")
const { Product } = require("db/products/collection")
const { Org } = require("db/organisations/collection")
const { OrgMembership } = require("db/org-memberships/collection")
const { Category, SubCategory } = require("db/categories/collection")
const { Template } = require("db/templates/collection")
const { TempInput } = require("db/template-inputs/collection")
const { TempInpAssoc } = require("db/template-input-associations/collection")

const OrgMembershipConstants = require("db/org-memberships/constants")
const TempInpConstants = require("db/template-inputs/constants")

const User = require("db/users/collection")

const app = require("middleware/app")
const { responses } = require("db/products/constants")
const config = require("config")
const supertest = require("supertest")

const productRouter = require("routes/products/route")()
const jwt = require("jsonwebtoken")

const _ = require("underscore")

const request = supertest(app)

const userData = {
  profile: {
    firstName: "Michael",
    lastName: "Perju",
    birthdate: new Date(),
  },
  email: "admin@app.com",
  password: "danPot9_ij",
}

const login = (user) => jwt.sign(
  { email: user.email, userId: user?._id },
  config.jwt.secret,
  {
    expiresIn: config.jwt.expiresIn,
  },
)

const createUser = async (data = userData) => new User(data).save()

const createOrg = async (user, data = { name: "test", role: OrgMembershipConstants.roles.OWNER }) => {
  if (!user) user = await createUser()

  const organisation = await new Org(data).save()
  const orgMembership = await new OrgMembership({
    userId: user._id,
    orgId: organisation._id,
    role: data.role,
  }).save()

  return { organisation, orgMembership }
}

const createCategory = async (data = { name: "Vehicles" }) => new Category(data).save()

const createSubCategory = async (category, data = { name: "Cars" }) => {
  if (!category) category = await createCategory()

  return new SubCategory({ categoryId: category._id, ...data }).save()
}

const createTemplateInputs = async () => {
  const stringInp = await new TempInput({ name: "string", dataType: TempInpConstants.fieldTypes.STRING }).save()
  const priceInp = await new TempInput({ name: "price", dataType: TempInpConstants.fieldTypes.PRICE }).save()
  const textInp = await new TempInput({ name: "text", dataType: TempInpConstants.fieldTypes.TEXT }).save()
  const numberInp = await new TempInput({ name: "number", dataType: TempInpConstants.fieldTypes.NUMBER }).save()
  const yearInp = await new TempInput({ name: "year", dataType: TempInpConstants.fieldTypes.YEAR }).save()
  const selectInp = await new TempInput({
    name: "select",
    dataType: TempInpConstants.fieldTypes.SELECT,
    meta: {
      select: {
        options: ["option-1", "option-2", "option-3"],
      },
    },
  }).save()

  return {
    stringInp,
    priceInp,
    textInp,
    numberInp,
    yearInp,
    selectInp,
  }
}

const createTestData = async () => {
  const subcategory = await createSubCategory()
  const user = await createUser()
  const orgRes = await createOrg(user, { name: "Test", role: OrgMembershipConstants.roles.ADMIN })
  const token = login(user)
  const templateInputs = await createTemplateInputs()
  return {
    subcategory,
    user,
    orgRes,
    token,
    templateInputs,
  }
}

describe("templates route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Product.deleteMany()
    await Org.deleteMany()
    await OrgMembership.deleteMany()
    await User.deleteMany()
    await Category.deleteMany()
    await SubCategory.deleteMany()
    await Template.deleteMany()
    await TempInput.deleteMany()
    await TempInpAssoc.deleteMany()
  })

  it("if the template has a string field requirement and the field is not provided it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.stringInp._id,
    }).save()

    // now the product has to follow the template and have the string input
    const res = await request.post("/")
      .send({
        organisationId: data.orgRes.organisation._id,
        subcategoryId: data.subcategory._id,
      })
      .set("Authorization", `Bearer ${data.token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.MISSING_TEMPLATE_FIELD)
  })

  it("if the template has a string field requirement and the field is longer than 100 characters it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.stringInp._id,
    }).save()

    // now the product has to follow the template and have the string input
    const res = await request.post("/")
      .send({
        organisationId: data.orgRes.organisation._id,
        subcategoryId: data.subcategory._id,
        data: {
          string: "thisIsAStringThatIsLongerThanTheLimitthisIsAStringThatIsLongerThanTheLimitthisIsAStringThatIsLongerThanTheLimit",
        },
      })
      .set("Authorization", `Bearer ${data.token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.INVALID_TEMPLATE_FIELD)
  })

  it("if the template has a number field requirement and the field is not provided it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.numberInp._id,
    }).save()

    // now the product has to follow the template and have the string input
    const res = await request.post("/")
      .send({
        organisationId: data.orgRes.organisation._id,
        subcategoryId: data.subcategory._id,
      })
      .set("Authorization", `Bearer ${data.token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.MISSING_TEMPLATE_FIELD)
  })

  it("if the template has a number field requirement and the field is not a number it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.numberInp._id,
    }).save()

    for (const number of ["k", "&", "47j", "_293"]) {
      // now the product has to follow the template and have the string input
      const res = await request.post("/")
        .send({
          organisationId: data.orgRes.organisation._id,
          subcategoryId: data.subcategory._id,
          data: {
            number,
          },
        })
        .set("Authorization", `Bearer ${data.token}`)
        .expect(400)

      expect(res.body.message).toBe(responses.INVALID_TEMPLATE_FIELD)
    }
  })

  it("if the template has a price field requirement and the field is not provided it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.priceInp._id,
    }).save()

    // now the product has to follow the template and have the string input
    const res = await request.post("/")
      .send({
        organisationId: data.orgRes.organisation._id,
        subcategoryId: data.subcategory._id,
      })
      .set("Authorization", `Bearer ${data.token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.MISSING_TEMPLATE_FIELD)
  })

  it("if the template has a price field requirement and the field is not an integer within [0-1000000] it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.priceInp._id,
    }).save()

    for (const price of ["90_", "ok", "47j", "_293", -200, "-8000", 1000001, -1, "-1"]) {
      // now the product has to follow the template and have the string input
      const res = await request.post("/")
        .send({
          organisationId: data.orgRes.organisation._id,
          subcategoryId: data.subcategory._id,
          data: {
            price,
          },
        })
        .set("Authorization", `Bearer ${data.token}`)
        .expect(400)

      expect(res.body.message).toBe(responses.INVALID_TEMPLATE_FIELD)
    }
  })

  it("if the template has a text field requirement and the field is not provided it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.textInp._id,
    }).save()

    // now the product has to follow the template and have the string input
    const res = await request.post("/")
      .send({
        organisationId: data.orgRes.organisation._id,
        subcategoryId: data.subcategory._id,
      })
      .set("Authorization", `Bearer ${data.token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.MISSING_TEMPLATE_FIELD)
  })

  it("if the template has a text field requirement and the field is longer than 5000 characters it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.textInp._id,
    }).save()

    // now the product has to follow the template and have the string input
    const res = await request.post("/")
      .send({
        organisationId: data.orgRes.organisation._id,
        subcategoryId: data.subcategory._id,
        data: {
          text: _.map(new Array(5000), () => "t").join(""),
        },
      })
      .set("Authorization", `Bearer ${data.token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.INVALID_TEMPLATE_FIELD)
  })

  it("if the template has a year field requirement and the field is not provided it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.yearInp._id,
    }).save()

    // now the product has to follow the template and have the string input
    const res = await request.post("/")
      .send({
        organisationId: data.orgRes.organisation._id,
        subcategoryId: data.subcategory._id,
      })
      .set("Authorization", `Bearer ${data.token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.MISSING_TEMPLATE_FIELD)
  })

  it("if the template has a year field requirement and the field is not an integer within 1500-2500 range it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.yearInp._id,
    }).save()

    for (const year of ["90_", "ok", "47j", "_293", -200, "-8000", 1499, "2501", -1, "-1"]) {
      // now the product has to follow the template and have the string input
      const res = await request.post("/")
        .send({
          organisationId: data.orgRes.organisation._id,
          subcategoryId: data.subcategory._id,
          data: {
            year,
          },
        })
        .set("Authorization", `Bearer ${data.token}`)
        .expect(400)

      expect(res.body.message).toBe(responses.INVALID_TEMPLATE_FIELD)
    }
  })

  it("if the template has a select field requirement and the field is not provided it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.selectInp._id,
    }).save()

    // now the product has to follow the template and have the string input
    const res = await request.post("/")
      .send({
        organisationId: data.orgRes.organisation._id,
        subcategoryId: data.subcategory._id,
      })
      .set("Authorization", `Bearer ${data.token}`)
      .expect(400)

    expect(res.body.message).toBe(responses.MISSING_TEMPLATE_FIELD)
  })

  it("if the template has a year field requirement and the field has no valid values it should fail", async () => {
    app.use("/", productRouter)

    const data = await createTestData()
    const template = await new Template({ subcategoryId: data.subcategory._id }).save()

    // associating a string input to the field just created
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: data.templateInputs.selectInp._id,
    }).save()

    for (const select of [[""], ["option1"], ["option-1", "option-4"]]) {
      // now the product has to follow the template and have the string input
      const res = await request.post("/")
        .send({
          organisationId: data.orgRes.organisation._id,
          subcategoryId: data.subcategory._id,
          data: {
            select,
          },
        })
        .set("Authorization", `Bearer ${data.token}`)
        .expect(400)

      expect(res.body.message).toBe(responses.INVALID_TEMPLATE_FIELD)
    }
  })
})
