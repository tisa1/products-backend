const mongoose = require("mongoose")
const { responses } = require("db/products/constants")
const { SubCategory } = require("db/categories/collection")
const { Template } = require("db/templates/collection")
const { Org } = require("db/organisations/collection")
const { OrgMembership } = require("db/org-memberships/collection")
const { fieldTypes } = require("db/template-inputs/constants")
const OrgMembershipConstants = require("db/org-memberships/constants")
const _ = require("underscore")

const validateField = (value, dataType, meta) => {
  if (dataType === fieldTypes.STRING) {
    return value?.length < 100
  }
  if (dataType === fieldTypes.TEXT) {
    return value?.length < 5000
  }
  if (dataType === fieldTypes.NUMBER) {
    return !Number.isNaN(Number(value))
  }
  if (dataType === fieldTypes.NUMBER) {
    return !Number.isNaN(Number(value))
  }
  if (dataType === fieldTypes.PRICE) {
    return !Number.isNaN(Number(value))
    && parseInt(value) > -1
    && parseInt(value) <= 1000000
  }
  if (dataType === fieldTypes.YEAR) {
    return !Number.isNaN(Number(value))
    && parseInt(value) >= 1500
    && parseInt(value) <= 2500
  }
  // if (dataType === fieldTypes.MULTISELECT) {
  //   return !_.difference(value, meta?.options)?.length
  // }
  if (dataType === fieldTypes.SELECT) {
    return _.includes(meta?.options, value)
  }

  return false
}

const create = async (req, res, next) => {
  // assume the userId is already given from jwt validation
  const userId = req?.decoded?.userId

  // the product should belong to an organisation
  const organisationId = req?.body?.organisationId
  if (!organisationId) return res.status(400).json({ message: responses.NO_ORGANISATION_ID })
  if (!mongoose.Types.ObjectId.isValid(organisationId)) {
    return res.status(400).json({ message: responses.ORGANISATION_ID_INVALID })
  }

  const doesOrganisationExist = await Org.findById(organisationId)
  if (!doesOrganisationExist) {
    return res.status(400).json({ message: responses.NO_ORGANISATION })
  }

  // does the user have access to the organisation?
  const doesUserBelongToOrg = await OrgMembership.countDocuments({
    orgId: organisationId,
    role: { $in: [OrgMembershipConstants.roles.OWNER, OrgMembershipConstants.roles.ADMIN] },
    userId,
  })

  if (!doesUserBelongToOrg) {
    return res.status(403).json({ message: responses.NO_ORGANISATION_MEMBERSHIP })
  }

  const subcategoryId = req?.body?.subcategoryId
  if (!subcategoryId) return res.status(400).json({ message: responses.NO_SUBCATEGORY_ID })

  if (!mongoose.Types.ObjectId.isValid(subcategoryId)) {
    return res.status(400).json({ message: responses.SUBCATEGORY_ID_INVALID })
  }

  const doesSubcategoryExist = await SubCategory.findById(subcategoryId)
  if (!doesSubcategoryExist) {
    return res.status(400).json({ message: responses.NO_SUBCATEGORY })
  }

  // validatin the product agains the template
  const templateRules = await Template.aggregate([
    {
      $match: { subcategoryId: mongoose.Types.ObjectId(subcategoryId) },
    },
    {
      $lookup: {
        from: "template-input-associations",
        let: { templateId: "$_id" },
        pipeline: [
          { $match: { $expr: { $eq: ["$templateId", "$$templateId"] } } },
          {
            $project: {
              templateId: 1,
              tempInputId: 1,
            },
          },
        ],
        as: "tempInputAssoc",
      },
    },
    {
      $project: {
        subcategoryId: 1,
        tempInputAssoc: 1,
      },
    },
    {
      $unwind: {
        path: "$tempInputAssoc",
        includeArrayIndex: "arrayIndex",
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $lookup: {
        from: "template-inputs",
        let: { templateId: "$tempInputAssoc.tempInputId" },
        pipeline: [
          { $match: { $expr: { $eq: ["$_id", "$$templateId"] } } },
          {
            $project: {
              name: 1,
              dataType: 1,
              meta: 1,
            },
          },
        ],
        as: "input",
      },
    },
    {
      $addFields: { input: { $arrayElemAt: ["$input", 0] } },
    },
    {
      $project: {
        input: 1,
      },
    },
  ])

  if (templateRules) {
    // get the data to be validated
    const productData = req?.body?.data

    for (const rule of templateRules) {
      const fieldName = rule?.input?.name
      const fieldDataType = rule?.input?.dataType
      const fieldMeta = rule?.input?.meta?.[fieldDataType]

      // is the field present?
      if (!productData?.[fieldName]) {
        return res.status(400).json({ message: responses.MISSING_TEMPLATE_FIELD })
      }

      // is the field of the right datatype?
      if (!validateField(productData?.[fieldName], fieldDataType, fieldMeta)) {
        return res.status(400).json({ message: responses.INVALID_TEMPLATE_FIELD })
      }
    }
  }

  return next()
}

const list = async (req, res, next) => next()

const listWithInputs = async (req, res, next) => {
  const templateId = req?.query?.templateId
  if (!templateId) return res.status(400).json({ message: responses.NO_TEMPLATE_ID })

  if (!mongoose.Types.ObjectId.isValid(templateId)) {
    return res.status(400).json({ message: responses.TEMPLATE_ID_INVALID })
  }

  const doesSubcategoryExist = await Template.findById(templateId)
  if (!doesSubcategoryExist) {
    return res.status(404).json({ message: responses.NO_TEMPLATE })
  }
  return next()
}

const remove = async (req, res, next) => {
  const templateId = req?.body?.templateId
  if (!templateId) return res.status(400).json({ message: responses.NO_TEMPLATE_ID })

  if (!mongoose.Types.ObjectId.isValid(templateId)) {
    return res.status(400).json({ message: responses.TEMPLATE_ID_INVALID })
  }

  const doesTemplateExist = await Template.findById(templateId)
  if (!doesTemplateExist) {
    return res.status(404).json({ message: responses.NO_TEMPLATE })
  }

  return next()
}

module.exports = {
  create,
  list,
  listWithInputs,
  remove,
}

// const data = {
//   subcategoryId: "xxx",
//   organisationId: "yyy",
//   data:{
//     name:"",
//     tipulCaroseriei
//   }
// }
