const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")

const { Product } = require("db/products/collection")
const { responses } = require("db/products/constants")

const jwtSecurity = require("core/jwt")
const routeSecurity = require("./security")
const middleware = require("./middleware")

const createRouter = () => {
  const productRouter = express.Router()

  productRouter.post("/", [
    jwtSecurity.checkToken,
    routeSecurity.create,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const data = req?.body
      const product = await new Product(data).save()

      return res.json({ message: responses.PRODUCT_CREATED, product })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on products / creates route",
      })
    }
  })

  productRouter.get("/", [
    routeSecurity.list,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const products = await Product.aggregate([
        {
          $lookup: {
            from: "categories",
            let: { subcategoryId: "$subcategoryId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$subcategoryId"] } } },
              {
                $project: {
                  name: 1,
                  categoryId: 1,
                },
              },
            ],
            as: "subcategory",
          },
        },
        {
          $addFields: {
            subcategory: { $arrayElemAt: ["$subcategory", 0] },
          },
        },
        {
          $lookup: {
            from: "categories",
            let: { categoryId: "$subcategory.categoryId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$categoryId"] } } },
              {
                $project: {
                  name: 1,
                },
              },
            ],
            as: "category",
          },
        },
        {
          $addFields: {
            category: { $arrayElemAt: ["$category", 0] },
          },
        },
        {
          $project: {
            name: 1,
            subcategory: 1,
            category: 1,
            data: 1,
          },
        },
      ])
      const total = await Product.countDocuments()

      return res.json({ products, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on products / list route",
      })
    }
  })

  productRouter.get("/single/with-inputs", [
    routeSecurity.listWithInputs,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const { productId } = req.query
      const products = await Product.aggregate([
        {
          $match: { _id: mongoose.Types.ObjectId(productId) },
        },
        {
          $lookup: {
            from: "product-input-associations",
            let: { productId: "$_id" },
            pipeline: [
              { $match: { $expr: { $eq: ["$productId", "$$productId"] } } },
              {
                $project: {
                  productId: 1,
                  tempInputId: 1,
                },
              },
            ],
            as: "tempInputAssoc",
          },
        },
        {
          $project: {
            subcategoryId: 1,
            tempInputAssoc: 1,
          },
        },
        {
          $unwind: {
            path: "$tempInputAssoc",
            includeArrayIndex: "arrayIndex",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "product-inputs",
            let: { productId: "$tempInputAssoc.tempInputId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$productId"] } } },
              {
                $project: {
                  name: 1,
                  type: 1,
                },
              },
            ],
            as: "input",
          },
        },
        {
          $addFields: { input: { $arrayElemAt: ["$input", 0] } },
        },
      ])

      return res.json({ products })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on products /single/with-inputs route",
      })
    }
  })

  productRouter.delete("/", [
    routeSecurity.remove,
  ], async (req, res, next) => {
    try {
      const productId = req?.body?.productId

      await Product.findByIdAndRemove(productId)
      return res.json({ message: responses.PRODUCT_DELETED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on products / delete route",
      })
    }
  })

  return productRouter
}

module.exports = createRouter
